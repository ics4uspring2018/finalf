import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The ExitButton object sends the user to the Level screen.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class StartButton extends Actor
{
    public void act() 
    {
        //Returns the user to the level screen if pressed
        if(Greenfoot.mousePressed(this))
        {
            Greenfoot.setWorld(new PlayerChoice());
        }
    }    
}