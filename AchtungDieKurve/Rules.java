import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Rules world displays the objective, controls, and powerups within the game.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class Rules extends World
{

    /**
     * Constructor for objects of class Rules.
     * Adds an exit button and displays the rules.
     */
    public Rules()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1000, 800, 1);

        Label display1 = new Label("", 80);
        Label display2 = new Label("", 80);
        Label display3 = new Label("", 80);
        Label display4 = new Label("", 80);
        Label display5 = new Label("", 80);
        Label display6 = new Label("", 80);
        Label display7 = new Label("", 80);
        Label display8 = new Label("", 80);
        Label display9 = new Label("", 80);
       
        display1.setImage(new GreenfootImage("How To Play: Stay alive and do not crash into other opponents. Every time an opponent", 30, Color.BLUE, Color.BLACK));
        display2.setImage(new GreenfootImage(" dies you will gain a point, first to 10 points wins. Press SPACE to start each round", 30, Color.BLUE, Color.BLACK));
        display3.setImage(new GreenfootImage("Controls: Blue player, Use J to turn left and L to turn right. ", 30, Color.BLUE, Color.BLACK));
        display4.setImage(new GreenfootImage("Red player, Use A to turn left and D to turn right.", 30, Color.BLUE, Color.BLACK));
        display5.setImage(new GreenfootImage("Powerups: Green powerups affect you, Red powerups affect others.", 30, Color.BLUE, Color.BLACK));
        display6.setImage(new GreenfootImage("Powerups last for two seconds.", 30, Color.BLUE, Color.BLACK));
        display7.setImage(new GreenfootImage("Makes player turn at 90 degrees", 25, Color.WHITE, Color.BLACK));
        display8.setImage(new GreenfootImage("Players speed increases", 25, Color.WHITE, Color.BLACK));
        display9.setImage(new GreenfootImage("Players speed decreases", 25, Color.WHITE, Color.BLACK));

        
        addObject(display1, 490, 200);
        addObject(display2, 465, 225);
        addObject(display3, 335, 270);
        addObject(display4, 275, 300);
        addObject(display5, 390, 350);
        addObject(display6, 400, 375);

        addObject(new SquarePowerup(), 100, 450);
        addObject(display7, 150, 500);
        addObject(new SpeedPowerup(), 500, 450);
        addObject(display8, 500, 500);
        addObject(new SlowPowerup(), 900, 450);
        addObject(display9, 850, 500);
        
        addObject(new ExitButton(), 300, 625);
        addObject(new RulesButton(), 500, 100);

    }
}
