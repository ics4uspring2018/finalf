import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The RulestButton object sends the user to the Rules screen.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class RulesButton extends Actor
{
    public void act() 
    {
        //Sends the user to the rules screen if pressed
        if(Greenfoot.mousePressed(this))
        {
            Greenfoot.setWorld(new Rules());
        }
    }
}
