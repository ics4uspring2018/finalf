import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The SpeedPowerup object is an in game powerup.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class SpeedPowerup extends Actor
{
    String type;
    
    /**
     * Constructor for objects of class SpeedPowerup
     * Sets the type and image randomly
     */
    public SpeedPowerup()
    {
        if(Greenfoot.getRandomNumber(2) == 1)
        {
            this.setImage("SpeedG.PNG");
            type = "green";
        }
        else
        {
            this.setImage("SpeedR.PNG");
            type = "red";
        }
    }
    
    /**
     * Returns the type of the powerup
     */
    public String check()
    {
        return type;
    }
}
