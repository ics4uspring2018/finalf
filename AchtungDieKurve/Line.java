import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Line object is a series of circles 'dropped' behind the player creating a line.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class Line extends Actor
{
    String colour; //The colour of the line
    int player; //The player that dropped the line
    int counter = 0; //How many acts since it was dropped
    
    
    /**
     * Constructor for objects of class Line
     * Sets the image based on the colour passed as a parameter
     */
    public Line(int player, String colour)
    {
        this.player = player;
        this.colour = colour;
        if(colour == "blue")
        {
            this.setImage("blueDot.png");
        }
        if(colour == "red")
        {
            this.setImage("redDot.png");
        }
        if(colour == "yellow")
        {
            this.setImage("yellowDot.png");
        }
    }
    
    public void act() 
    {
        counter++;
    }
    
    /**
     * Returns the player that dropped the line
     */
    public int getPlayer()
    {
        return player;
    }
    
    /**
     * Returns how long ago the line was dropped
     */
    public int getTime()
    {
        return counter;
    }
}
