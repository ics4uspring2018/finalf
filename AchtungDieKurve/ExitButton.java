import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The ExitButton object returns the user to the start screen.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class ExitButton extends Actor
{
    public void act() 
    {
        //Returns the user to the start screen if pressed
        if(Greenfoot.mousePressed(this))
        {
            Greenfoot.setWorld(new StartScreen());
        }
    }    
}
