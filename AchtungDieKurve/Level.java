import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Level world is where the game is run.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class Level extends World
{
    int x1; //Player 1 x-coordinate
    int x2; //Player 2 x-coordinate
    int y1; //Player 1 y-coordinate
    int y2; //Player 2 y-coordinate
    int p1score; //Player 1 score
    int p2score; //Player 2 score
    int delay = 150; //Delay before switching worlds
    int counter = 0; //Tracks how many acts have passed
    int players; //The amount of players in the game
    boolean gameOver = false; //Tracks if the game is still running
    boolean p1alive = true; //Tracks if player 1 is alive
    boolean p2alive = true; //Tracks if player 2 is alive
    boolean p2aialive = true; //Tracks if player 2 ai is alive
    boolean gameEnded = false; //Tracks if the end game code has been run
    boolean resetGame = false; //Tracks if the game need to be reset
    Leaderboard lone; //Leaderboard
    Label display = new Label("", 80); //Displays text
    Player p1; //Player 1
    Player p2; //Player 2
    Player p2ai; //Player 2 AI

    /**
     * Constructor for objects of class MyWorld.
     * Randomly sets the coordinates of the two players and adds them to the world.
     * 
     * Takes the score of both players as parameters.
     * When a new Level is created the current score is passed.
     * This allows the players' score to keep updating
     */
    public Level(int p1score, int p2score, int players)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1000,800, 1);
        //Updates the score of the players.
        this.p1score = p1score;
        this.p2score = p2score;
        this.players = players;
        //Sets random coordinates for both players
        x1 = Greenfoot.getRandomNumber(700) + 50;
        x2 = Greenfoot.getRandomNumber(700) + 50;
        y1 = Greenfoot.getRandomNumber(700) + 50;
        y2 = Greenfoot.getRandomNumber(700) + 50;
        //Sets the order so that the player is displayed above the line
        setPaintOrder(Label.class, Player.class, Line.class);
        //Ensures that both players are not spawned too close to each other
        while((x2 > x1 - 10 && x2 < x1 + 10) && (y2 > y1 - 10 && y2 < y1 + 10))
        {
            x2 = Greenfoot.getRandomNumber(700) + 50;
            y2 = Greenfoot.getRandomNumber(700) + 50;
        }
        //Creates the players and adds them to the world
        p1 = new Player(x1, y1, 1, "a", "d", "red", false);
        p2 = new Player(x2, y2, 2, "j", "l", "blue", false);
        p2ai = new Player(x2, y2, 2, "1", "2", "blue", true);
        addObject(p1, x1, y1); 
        if(players == 2)
        {
            addObject(p2, x2, y2);
        }
        if(players == 1)
        {
            addObject(p2ai, x2, y2);
        }
        //Adds a leaderboard to the world
        lone = new Leaderboard(2);
        addObject(lone, 0, 0);
    }

    public void act()
    {
        //Updates the leaderboard scores
        if(counter == 0)
        {
            lone.addPoint(1, p1score);
            lone.addPoint(2, p2score);
        }
        counter++;
        //Only runs if the game is still running
        if(!gameEnded)
        {
            //If either player is dead the game is over
            if(!p1alive || !p2alive || !p2aialive)
            {
                gameOver = true;
            }
            //Only runs if the game is over
            if(gameOver)
            {
                //Both players are stopped
                p1.stopPlayer();
                p2.stopPlayer();
                p2ai.stopPlayer();
                //Depending on which player remained alive, the scores aarae updated
                if(!p1alive)
                {
                    p2score++;
                    updateScore(2);
                }
                if(!p2alive || !p2aialive)
                {
                    p1score++;
                    updateScore(1);
                }
                //The end game code has been run, so game ended is set to true
                gameEnded = true;
            }
        }
        //Once the game is ended, teh next game will start once space is pressed
        if(gameEnded && !resetGame)
        {
            if(Greenfoot.isKeyDown("space"))
            {
                Greenfoot.setWorld(new Level(p1score, p2score, players));
            }
        }
        //If a player has 10 points the game is over and a win message appears
        //The game then returns to the start screen
        if(p1score > 9 && !resetGame)
        {
            addObject(display, 450, 400);
            display.setImage(new GreenfootImage("Player 1 wins!", 60, Color.RED, Color.BLACK));
            p1.stopPlayer();
            if(players == 2)
            {
                p2.stopPlayer();                
            }
            else
            {
                p2ai.stopPlayer();
            }
            resetGame = true;
        }
        if(p2score > 9 && !resetGame)
        {
            addObject(display, 450, 400);
            display.setImage(new GreenfootImage("Player 2 wins!", 60, Color.BLUE, Color.BLACK));
            p1.stopPlayer();
            p2.stopPlayer();
            resetGame = true;
        }
        if(resetGame)
        {
            if(delay < 0)
            {
                Greenfoot.setWorld(new StartScreen());
            }
            delay--;
        }
    }

    /**
     * Updates the leadboard score.
     * Takes winner as a parameter: the player that won the game.
     * The winner's score is increased by one point.
     */
    public void updateScore(int winner)
    {
        lone.addPoint(winner, 1);
    }

    /**
     * Updates which players are dead.
     * Takes dead as a parameter: the player that died.
     * Their boolean statused are updated correspondingly.
     */
    public void updateDead(int dead)
    {
        if(dead == 1)
        {
            p1alive = false;
        }
        if(dead == 2)
        {
            if(players == 2)
            {
                p2alive = false;
            }
            else
            {
                p2aialive = false;
            }
        }
    }

    /**
     * Increases the speed of whichever player hit the speed powerup.
     * The player will go at 1.5 speed for 2 seconds.
     */
    public void speed(int player)
    {
        if(player == 1)
        {
            if(players == 2)
            {
                p2.triggerSpeed();
            }
            else
            {
                p2ai.triggerSpeed();
            }
        }
        else
        {
            p1.triggerSpeed();
        }
    }

    /**
     * Decreases the speed of the player that got the slow powerup
     * The player will go at 0.5 speed for 2 seconds.
     */
    public void slow(int player)
    {
        if(player == 1)
        {
            if(players == 2)
            {
                p2.triggerSlow();
            }
            else
            {
                p2ai.triggerSlow();
            }
        }

        else
        {
            p1.triggerSlow();
        }
    }

    /**
     * Changes the turn angle of the player who got the square powerup.
     * The player will turn at an angle 90 degrees for 2 seconds.
     */
    public void square(int player)
    {
        if(player == 1)
        {
            if(players == 2)
            {
                p2.triggerSquare();
            }
            else
            {
                p2ai.triggerSquare();
            }
        }
        else
        {
            p1.triggerSquare();
        }
    }
}
