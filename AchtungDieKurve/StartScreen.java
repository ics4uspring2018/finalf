import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The StartScreen world contains buttons to the game and the rules.
 * 
 * Instructions
 * This is primarily a two player local multiplayer game.
 * We have added in a one player version with a basic AI.
 * This will help you test the functionality of the game.
 * 
 * Features:
 * - Two Player gameplay
 * - One Player vs CPU gameplay
 * - 6 unique powerups
 * - Scoring, winner is first to ten points
 * - Smooth manoeuvring
 * - Engaging and intense gameplay
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class StartScreen extends World
{

    /**
     * Constructor for objects of class StartScreen.
     * Adds a rules button, start button and displays the title.
     */
    public StartScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1000, 800, 1); 
        addObject(new StartButton(), 300, 625);
        addObject(new RulesButton(), 700, 615);
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        Title title2 = new Title();
        addObject(title2,500,255);
    }
}
