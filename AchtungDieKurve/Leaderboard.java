import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Leaderboard object displays the player's scores.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class Leaderboard extends Actor
{
    int counter = 0; //Tracks how many acts have occured
    int redScore = 0; //Tracks player one's score
    int blueScore = 0; //Tracks player two's score    
    int players;
    //Two labels to diaply scores
    Label display1 = new Label("", 80);
    Label display2 = new Label("", 80);

    public Leaderboard(int players)
    {
        this.players = players;
    }

    public void act() 
    {
        counter++;
        //Draws the yellow border around the game
        if(counter == 1)
        {
            for(int i = 0; i < 800; i += 2)
            {
                this.getWorld().addObject(new Line(100, "yellow"), 800, i);
            }
            for(int i = 0; i < 800; i += 2)
            {
                this.getWorld().addObject(new Line(100, "yellow"), 3, i);
            }
            for(int i = 0; i < 800; i += 2)
            {
                this.getWorld().addObject(new Line(100, "yellow"), i, 3);
            }
            for(int i = 0; i < 800; i += 2)
            {
                this.getWorld().addObject(new Line(100, "yellow"), i, 797);
            }

            getWorld().addObject(display1, 900, 300);
            getWorld().addObject(display2, 900, 400);

        }
        //Displays the scores
        display1.setImage(new GreenfootImage("Red: " + redScore, 60, Color.RED, Color.BLACK));
        display2.setImage(new GreenfootImage("Blue: " + blueScore, 60, Color.BLUE, Color.BLACK));
    }

    /**
     * Adds an amount of points to a player's score.
     */
    public void addPoint(int player, int val)
    {
        if(player == 1)
        {
            redScore += val;
        }
        if(player == 2)
        {
            blueScore += val;
        }
    }
}
