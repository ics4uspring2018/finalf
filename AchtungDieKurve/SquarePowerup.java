import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The SquarePowerup object is an in game powerup.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class SquarePowerup extends Actor
{
    String type;
    
    /**
     * Constructor for objects of class SquarePowerup
     * Sets the type and image of powerup randomly between Green and Red
     */
    public SquarePowerup()
    {
        
        if(Greenfoot.getRandomNumber(2) == 1)
        {
            this.setImage("SquareG.PNG");
            type = "green";
        }
        else
        {
            this.setImage("SquareR.PNG");
            type = "red";
        }
    }

    /**
     * Returns the type of the powerup
     */
    public String check()
    {
        return type;
    }    
}
