import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class TwoPlayerButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TwoPlayerButton extends Actor
{
    /**
     * Act - do whatever the TwoPlayerButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(Greenfoot.mousePressed(this))
        {
            Greenfoot.setWorld(new Level(0, 0, 2));
        }
    }    
}
