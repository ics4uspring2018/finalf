import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The SloePowerup object is an in game powerup.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class SlowPowerup extends Actor
{
    String type;

    /**
     * Constructor for objects of class SlowPowerup
     *Sets the type and image of powerup randomly between Green and Red
     */
    public SlowPowerup()
    {
        if(Greenfoot.getRandomNumber(2) == 1)
        {
            this.setImage("SlowG.PNG");
            type = "green";
        }
        else
        {
            this.setImage("SlowR.PNG");
            type = "red";
        }
    }

    /**
     * Returns the type of the powerup
     */
    public String check()
    {
        return type;
    }
}   

