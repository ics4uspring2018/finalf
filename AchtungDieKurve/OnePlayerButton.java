import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class OnePlayerButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class OnePlayerButton extends Actor
{
    /**
     * Act - do whatever the OnePlayerButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(Greenfoot.mousePressed(this))
        {
            Greenfoot.setWorld(new Level(0, 0, 1));
        }
    }    
}
