import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class PlayerChoice here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlayerChoice extends World
{
    Label display1 = new Label("", 80);
    /**
     * Constructor for objects of class PlayerChoice.
     * 
     */
    public PlayerChoice()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1000, 800, 1); 
        addObject(new OnePlayerButton(), 300, 600);
        addObject(new TwoPlayerButton(), 700, 600);     
        addObject(display1, 500, 400);
        display1.setImage(new GreenfootImage("Players:", 70, Color.WHITE, Color.BLACK));
    }
}
