import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;
/**
 * The Player object is a user controlled object.
 * 
 * @authors Ryan Nish, Taha Qadir, Brandon Hewitson
 * @version June 2018
 */
public class Player extends Actor
{
    String leftKey; //The key that turns the player left
    String rightKey; //The key that turns the player right
    String colour; //The colour of the player
    boolean dead = false; //Tracks if the player is alive
    boolean active = false; //Tracks if the player is active
    boolean drawnLine = false; //Tracks if the player's starting line has been drawn
    boolean ai; //Tracks if the player is user controlled
    int player; //The integer value of the player
    int counter = 0; //Tracks when to create the next gap
    int gapCount = 0; //Tracks when to stop creating a gap in the line
    int count = 0; //Tracks how many acts have passed
    int turn = 3; //Tracks how many degrees the player should turn
    int cooldown = -1; //Tracks how long before the player can turn 90 degrees again
    //Helps with ai
    int cooldown2 = -1;
    int cooldown3 = -1;
    int cooldown4 = -1;
    int direction = 1;
    int direction2 = 1;
    int detected = 0;
    int speedCounter = -1; //Tracks how long the speed power should last
    int slowCounter = -1; //Tracks how long the slow powerup should last
    int squareCounter = -1; //Tracks how long the square powerup should last
    int x; //The x-coordinate of the player
    int y; //The y coordinate of the player
    //Accounts for the loss of precision when rounding movement using trigonometry
    double xLeftover = 0.0; 
    double yLeftover = 0.0;    
    double speed = 1.7; //The player's speed

    /**
     * Constructor for objects of class Player.
     * Takes the x-coordinate, y-coordinate, left movement key, right movement key and colour as parameters.
     * A random rotation is set.
     */
    public Player(int x, int y, int player, String leftKey, String rightKey, String colour, boolean ai)
    {
        this.player = player;
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.colour = colour;
        this.ai = ai;
        this.x = x;
        this.y = y;
        setRandomRotation();
    }

    public void act()
    {
        //Draws the starting line for player if it hasn;t been drawn already
        if(!drawnLine)
        {
            makeStartingLine();
        }
        //Checks if the player is pressing space the start the game
        checkActivate();
        //Updates the act counter if the game is running
        if(active)
        {
            count++;
        }
        //Updates the level and leaderboard if the player is dead
        if(dead)
        {
            Level l = (Level) this.getWorld();
            l.updateDead(player);
            dead = false;
            active = false;
        }
        //If the player is active and not dead the normal operations are carried out
        if(!dead && active)
        {
            manageGap();
            if(!ai)
            {
                checkTurn();
            }
            else
            {
                aiTurn();
            }
            if(count > 10)
            {
                checkCollision();
            }
            checkPowerup();
            spawnPowerup();
            updateSpeed();
            updateSlow();
            updateSquare();
            checkBorder();
            move();
        }
    }

    /**
     * Incorporates the basic code for a move method along with extra code to 
     * allow for rounding to be carried over throughout acts. This creates more fluid movement.
     */
    public void move()
    {
        double radians = Math.toRadians(getRotation());
        double x = Math.cos(radians) * speed;
        int dx = (int) Math.round(Math.cos(radians) * speed);
        xLeftover += (double)(x - (double)dx);
        double y =  Math.sin(radians) * speed;
        int dy = (int) Math.round(Math.sin(radians) * speed);
        yLeftover += (double)(y - (double)dy);
        this.setLocation(getX() + dx, getY() + dy);
        if(xLeftover > 1)
        {
            this.setLocation(getX() + 1, getY());
            xLeftover--;
        }
        if(xLeftover < 1)
        {
            this.setLocation(getX() - 1, getY());
            xLeftover++;
        }
        if(yLeftover > 1)
        {
            this.setLocation(getX(), getY() + 1);
            yLeftover--;
        }
        if(yLeftover < 1)
        {
            this.setLocation(getX(), getY() - 1);
            yLeftover++;
        }
    }

    /**
     * Sets the rotation of the player to a random orientation
     */
    public void setRandomRotation()
    {
        this.setRotation(Greenfoot.getRandomNumber(360));
    }

    /**
     * Checks if the user is activating the player
     */
    public void checkActivate()
    {
        if(Greenfoot.isKeyDown("space"))
        {
            active = true;
        }
    }

    /**
     * Manages the player's line being drawn
     */
    public void manageGap()
    {
        if(gapCount == 0)
        {
            counter = Greenfoot.getRandomNumber(120) + 120; //Randomizes how long before the next gap
        }
        counter--;
        gapCount--;
        //When counter runs out a gap is created for 15 acts
        if(counter == 0)
        {
            gapCount = 15;
        }
        //If not currently gapping, a line is created behind the player
        if(gapCount < 0)
        {
            addLine();
        }
    }

    /**
     * Checks if the user is turning the player.
     */
    public void checkTurn()
    {
        //Turning occurs as normal unless the square powerup is active.
        //If so, there is a delay between turning.
        if(Greenfoot.isKeyDown(leftKey) && cooldown < 0)
        {
            turn(-turn);
            if(turn == 90)
            {
                cooldown = 12;
            }
        }
        if(Greenfoot.isKeyDown(rightKey) && cooldown < 0)
        {
            turn(turn);
            if(turn == 90)
            {
                cooldown = 12;
            }
        }
        if(cooldown > -1)
        {
            cooldown--;
        }
    }

    /**
     * Checks to see if the player is colliding with a line.
     * If so, the player is set to dead.
     */
    public void checkCollision()
    {
        Actor b = getOneIntersectingObject(Line.class);  
        Line line = (Line) b;
        if(b != null && (line.getPlayer() != player || line.getTime() > 10))
        {  
            dead = true;
        }  
    }

    /**
     * Adds a line obect at the player's current coordinates.
     */
    public void addLine()
    {
        this.getWorld().addObject(new Line(player, colour), this.getX(), this.getY());
        this.setLocation(getX(), getY());
    }

    /**
     * Checks if the player is hitting the border of the game area.
     * If so, the player is set to dead.
     */
    public void checkBorder()
    {
        if(this.getX() < 5 || this.getX() > 795 || this.getY() < 5 || this.getY() > 795)
        {
            dead = true;
        }
    }

    /**
     * Randomly decides whether to spawn a powerup, and if so which powerup.
     * The coordinates of the powerup are set randomly.
     */
    public void spawnPowerup()
    {
        int b = player + 57;
        if(Greenfoot.getRandomNumber(800) == b)
        {
            int a = Greenfoot.getRandomNumber(3);
            if(a == 1)
            {
                getWorld().addObject(new SpeedPowerup(), Greenfoot.getRandomNumber(700) + 50, Greenfoot.getRandomNumber(700) + 50);
            }
            if(a == 2)
            {
                getWorld().addObject(new SquarePowerup(), Greenfoot.getRandomNumber(700) + 50, Greenfoot.getRandomNumber(700) + 50);
            }
            getWorld().addObject(new SlowPowerup(), Greenfoot.getRandomNumber(700) + 50, Greenfoot.getRandomNumber(700) + 50);
        }
    }

    /**
     * The line behind the player at the start of the game is created.
     */
    public void makeStartingLine()
    {
        drawnLine = true;
        double radians = Math.toRadians(getRotation() + 180);
        int dx = (int) Math.round(Math.cos(radians) * speed);
        int dy = (int) Math.round(Math.sin(radians) * speed);
        for (int i = 0; i < 16; i++)
        {
            getWorld().addObject(new Line(player, colour), x + dx * i, y + dy * i);
        }
    }

    /**
     * Checks if the player is colliding with any powerups.
     * The powerups are then triggered accordingly.
     */
    public void checkPowerup()
    {
        Actor b = getOneIntersectingObject(SpeedPowerup.class);
        if(b != null)
        {  
            SpeedPowerup s = (SpeedPowerup) b;
            if(s.check() == "green")
            {
                triggerSpeed();
            }
            else
            {
                Level l1 = (Level) this.getWorld();
                l1.speed(player);
            }
            getWorld().removeObject(b);
        }         
        Actor c = getOneIntersectingObject(SlowPowerup.class);  
        if(c != null)
        {  
            SlowPowerup r = (SlowPowerup) c;
            if(r.check() == "green")
            {
                triggerSlow();
            }
            else
            {
                Level l2 = (Level) this.getWorld();
                l2.slow(player);
            }
            getWorld().removeObject(c);
        } 
        Actor d = getOneIntersectingObject(SquarePowerup.class);  
        if(d != null)
        {  
            SquarePowerup q = (SquarePowerup) d;
            if(q.check() == "green")
            {
                triggerSquare();
            }
            else
            {
                Level l3 = (Level) this.getWorld();
                l3.square(player);
            }
            getWorld().removeObject(d);
        } 
    }

    /**
     * Stops the player's movement.
     */
    public void stopPlayer()
    {
        active = false;
    }

    /**
     * Increases the player's speed for 2 seconds.
     */
    public void triggerSpeed()
    {
        speed = 4;
        speedCounter = 120;
    }

    /**
     * Decreases the player's speed for 2 seconds.
     */
    public void triggerSlow()
    {
        speed = 1;
        slowCounter = 120;
    }

    /**
     * Changes the player's turn angle to 90 degrees for 2 seconds.
     */
    public void triggerSquare()
    {
        turn = 90;
        squareCounter = 120;
    }

    /**
     * Resets the player's speed after the speed powerup has expired.
     */
    public void updateSpeed()
    {
        if(speedCounter > 0)
        {
            speedCounter--;
        }
        else
        {
            if(slowCounter < 0)
            {
                speed = 2;
            }
        }
    }

    /**
     * Resets the player's speed after the slow powerup has expired.
     */
    public void updateSlow()
    {
        if(slowCounter > 0)
        {
            slowCounter--;
        }
        else
        {
            if(speedCounter < 0)
            {
                speed = 2;
            }
        }
    }

    /**
     * Resets the player's turn angle after the powerup has expired
     */
    public void updateSquare()
    {
        if(squareCounter > 0)
        {
            squareCounter--;
        }
        else
        {
            turn = 3;
        }
    }

    /**
     * Basic ai movement.
     * Uses random turning along with a basic collision detector to avoid simple collisions
     */
    public void aiTurn()
    {
        if(squareCounter > 0)
        {
            if(cooldown3 < 0)
            {
                cooldown3 = Greenfoot.getRandomNumber(10) + 15;
            }
            int b = Greenfoot.getRandomNumber(2);
            if(b == 1 && cooldown3 == 0)
            {
                turn(90);
            }
            else if(b == 1 && cooldown3 == 0)
            {
                turn(-90);
            }
            cooldown3--;
        }
        else
        {
            if(collisionDetected(60))
            {
                detected++;
                if(detected > 60)
                {
                    if(direction2 == 0)
                    {
                        direction2 = 1;
                    }
                    else
                    {
                        direction2 = 0;
                    }
                    detected = 0;
                }
                else
                {
                    cooldown4--;
                }
                if(direction2 == 1)
                {
                    turn(3);
                }
                if(direction2 == 0)
                {
                    turn(-3);
                }
            }
            else
            {
                detected = 0;
                if(cooldown < 0)
                {
                    cooldown = Greenfoot.getRandomNumber(20) + 5;
                    direction = Greenfoot.getRandomNumber(3);
                }
                else
                {
                    cooldown--;
                }
                if(direction == 1)
                {
                    turn(3);
                }
                else if(direction == 0)
                {
                    turn(-3);
                }
            }
        }
    }
    
    /**
     * Checks if there is a Line within a certain radius
     */
    public boolean collisionDetected(int radius)
    {
        List list = getObjectsInRange(radius, Line.class);
        if(list == null)
        {
            return false;
        }
        for(int i = 0; i < list.size(); i++)
        {
            Line l = (Line) list.get(i);            
            if(l.getTime() > 60 || l.getPlayer() != 2)
            {                
                return true;
            }
        }
        return false;
    }
}